const { MongoClient } = require("mongodb");

function connectToServer() {
  return new Promise((resolve, reject) => {
    const URL = "mongodb://localhost:27017";
    MongoClient.connect(URL, {
      useUnifiedTopology: true,
    })
      .then((client) => {
        resolve(client);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

module.exports = {
  connectToServer,
};
