const { ObjectID } = require("mongodb");
const { Router } = require("express");
const { connectToServer } = require("../data/DBConnection");

const router = new Router();

router.get("/", (req, res) => {
  connectToServer()
    .then((client) => {
      const db = client.db("demo");
      const users = db.collection("users");
      const userCursor = users.find({});

      userCursor.toArray().then((data) => {
        res.json(data);
        client.close();
      });
    })
    .catch((err) => res.send(err));
});

router.post("/", (req, res) => {
  connectToServer()
    .then((client) => {
      const db = client.db("demo");
      const users = db.collection("users");

      const userToInsert = req.body;

      users.insertOne(userToInsert).then((newUser) => {
        res.status(201).json({ _id: newUser.insertedID, ...userToInsert });
        client.close();
      });
    })
    .catch((err) => res.send(err));
});

router.get("/:userID", (req, res) => {
  connectToServer()
    .then((client) => {
      const db = client.db("demo");
      const users = db.collection("users");

      users.findOne({ _id: ObjectID(req.params.userID) }).then((user) => {
        res.json(user);
        client.close();
      });
    })
    .catch((err) => res.send(err));
});

module.exports = router;
